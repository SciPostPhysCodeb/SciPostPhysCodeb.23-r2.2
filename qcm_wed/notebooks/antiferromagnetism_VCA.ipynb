{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# VCA for antiferromagnetism\n",
    "\n",
    "Find the VCA solution for Néel antiferromagnetism in the nearest-neighbor, particle-hole symmetric Hubbard model at half-filling, using **two** $3\\times 3$ clusters embedded in a two-dimensional square lattice.\n",
    "Use only one variational parameter: the Weiss field $M$, defined as the coefficient of the antiferromagnetic operator. Plot the order parameter $\\langle M\\rangle$ as a function of $U$, from $U=10$ to $U=0$, in steps of $\\Delta U=0.5$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyqcm\n",
    "from pyqcm.vca import VCA\n",
    "import numpy as np\n",
    "\n",
    "# declare a cluster model of 9 sites, named 'clus'\n",
    "CM = pyqcm.cluster_model( 9)  \n",
    "\n",
    "sites = []\n",
    "for j in range(3):\n",
    "    for i in range(3):\n",
    "        sites.append((i,j,0))\n",
    "\n",
    "# defining a hopping operator that only hops around the perimeter of the clusters\n",
    "ns = 9\n",
    "CM.new_operator('tperim', 'one-body',[\n",
    "    (1, 2, -1.0), \n",
    "    (2, 3, -1.0),\n",
    "    (3, 6, -1.0), \n",
    "    (6, 9, -1.0),\n",
    "    (8, 9, -1.0), \n",
    "    (7, 8, -1.0),\n",
    "    (4, 7, -1.0), \n",
    "    (1, 4, -1.0),\n",
    "    (1+ns, 2+ns, -1.0), \n",
    "    (2+ns, 3+ns, -1.0),\n",
    "    (3+ns, 6+ns, -1.0), \n",
    "    (6+ns, 9+ns, -1.0),\n",
    "    (8+ns, 9+ns, -1.0), \n",
    "    (7+ns, 8+ns, -1.0),\n",
    "    (4+ns, 7+ns, -1.0), \n",
    "    (1+ns, 4+ns, -1.0)\n",
    "])\n",
    "\n",
    "# define a physical cluster based on that model, with base position (0,0,0) and site positions\n",
    "clus1 = pyqcm.cluster(CM, sites) \n",
    "clus2 = pyqcm.cluster(CM, sites, (3,0,0)) # second cluster, offset from the first\n",
    "\n",
    "# define a lattice model named '1D_4' made of the cluster(s) clus and superlattice vector (4,0,0)\n",
    "model = pyqcm.lattice_model('model_3x3_2C', (clus1, clus2), ((6,0,0),(1,3,0)))\n",
    "\n",
    "# define a few operators in this model\n",
    "model.interaction_operator('U')\n",
    "model.hopping_operator('t', (1,0,0), -1)\n",
    "model.hopping_operator('t', (0,1,0), -1)\n",
    "model.density_wave('M', 'Z', (1,1,0)) # Spin density wave at Q = (pi,pi)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.draw_operator('t')\n",
    "model.draw_cluster_operator(model.clus[0], 'tperim')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Setting target sectors with S=1 and S=-1 such that S_total = 0\n",
    "model.set_target_sectors(('R0:N9:S-1', 'R0:N9:S1')) # half filling for both clusters\n",
    "model.set_parameters(\"\"\"\n",
    "t = 1\n",
    "U = 8\n",
    "mu = 0.5*U\n",
    "M = 0\n",
    "M_1 = 0.1\n",
    "M_2 = 1*M_1\n",
    "t_1 = 1\n",
    "t_2 = 1*t_1\n",
    "tperim_1 = 1e-9\n",
    "tperim_2 = 1*tperim_1\n",
    "\"\"\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Estimating the starting value of `M_1` \n",
    "Here we make a rough plot of the self-energy functional as a function of `M_1` to find a good starting point for the VCA procedure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.set_parameter(\"U\", 10)\n",
    "pyqcm.vca.plot_sef(model, 'M_1', np.arange(0.05, 0.151, 0.01))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Controlled VCA loop over $U$\n",
    "Here we loop over $U$ in the range from 10 to 0 and perform the VCA with `M_1` as a variational parameter. Looping downwards over U allows for easier convergence on a solution since no\n",
    "We use the function `controlled_loop()` to benefit from predictors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.set_parameter(\"M_1\", 0.11) # initial guess for M_1 based on the sef\n",
    "I = pyqcm.model_instance(model)\n",
    "\n",
    "def run_vca():\n",
    "    V = VCA(model, varia='M_1', steps=0.005, accur=2e-3, max=10, method='altNR')\n",
    "    return V.I\n",
    "\n",
    "# controlled_loop performs successive VCAs with starting value prediction\n",
    "model.controlled_loop(run_vca, varia=[\"M_1\"], loop_param=\"U\", loop_range=(10, -0.1, -0.5)) # loops from 10 to 0 in increments of 0.5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### VCA for $U=8$ with `M_1` and `t_1` as variational parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.set_parameter(\"U\", 8)\n",
    "V = VCA(model, varia=('M_1', 't_1'), start=(0.17, 1.15), steps=(0.005, 0.005), accur=(2e-3, 2e-3), max=(10, 10)) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### VCA for $U=8$ with `M_1`, `t_1` and `tperim_1` as variational parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "V = VCA(model, varia=('M_1', 't_1', 'tperim_1'), start=(0.17, 1.15, 0.04), steps=(0.005, 0.005, 0.005), accur=(2e-3, 2e-3, 2e-3), max=(10, 10, 10))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Getting data from prerun VCAs\n",
    "d_M = np.genfromtxt(\"./example_data/EX2_data_M.tsv\", names=True)\n",
    "d_M_t = np.genfromtxt(\"./example_data/EX2_data_M_t.tsv\", names=True)\n",
    "d_M_t_tperim = np.genfromtxt(\"./example_data/EX2_data_M_t_tperim.tsv\", names=True)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plotting $|\\langle M \\rangle|$ as a function of $U$ for all three simulations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "\n",
    "ax.plot(d_M[\"U\"], np.abs(d_M[\"ave_M\"]), \"s\", markersize=3, label=\"M_1\")\n",
    "ax.plot(d_M_t[\"U\"], np.abs(d_M_t[\"ave_M\"]), \"s\", markersize=3, label=\"M_1 & t_1\")\n",
    "ax.plot(d_M_t_tperim[\"U\"], np.abs(d_M_t_tperim[\"ave_M\"]), \"s\", markersize=3, label=\"M_1, t_1 & tperim_1\")\n",
    "\n",
    "ax.legend(loc=\"upper left\")\n",
    "\n",
    "ax.set_xlabel(\"U\")\n",
    "ax.set_ylabel(\"$|\\langle M\\\\rangle|$\")\n",
    "\n",
    "subax = fig.add_axes([0.5, 0.25, 0.35, 0.35])\n",
    "\n",
    "subax.plot(d_M[\"U\"][14:17], np.abs(d_M[\"ave_M\"][14:17]), \"s\", markersize=3)\n",
    "subax.plot(d_M_t[\"U\"], np.abs(d_M_t[\"ave_M\"]), \"s\", markersize=3)\n",
    "subax.plot(d_M_t_tperim[\"U\"], np.abs(d_M_t_tperim[\"ave_M\"]), \"s\", markersize=3)\n",
    "\n",
    "subax.set_xlabel(\"U\")\n",
    "subax.set_ylabel(\"$|\\langle M\\\\rangle|$\")\n",
    "\n",
    "subax.set_xticks((d_M[\"U\"][14:17]))\n",
    "\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plotting the Weiss field as a function of $U$ for all three simulations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "\n",
    "ax.plot(d_M[\"U\"], d_M[\"M_1\"], \"s\", markersize=3, label=\"M_1\")\n",
    "ax.plot(d_M_t[\"U\"], d_M_t[\"M_1\"], \"s\", markersize=3, label=\"M_1 & t_1\")\n",
    "ax.plot(d_M_t_tperim[\"U\"], d_M_t_tperim[\"M_1\"], \"s\", markersize=3, label=\"M_1, t_1 & tperim_1\")\n",
    "\n",
    "ax.legend(loc=\"lower right\")\n",
    "\n",
    "ax.set_xlabel(\"U\")\n",
    "ax.set_ylabel(\"|M_1|\")\n",
    "\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Interpretation\n",
    "\n",
    "In the first figure, we can see how $|\\langle M \\rangle|$ seems to approach 1 relatively quickly as a function of $U$. On the other hand, $|M\\_1|$ seems to peak around $U\\approx 5$ (fig.2). This means that as $U$ rises above 5, a rise in order parameter actually requires a *diminishing* Weiss field due to the rising interaction strength.\n",
    "\n",
    "In both figures, adding in t_1 to the variational parameters seems to cause a rise in the values. However, adding in tperim_1 only causes a very minute increase. This makes sense since there doesn't seem to be a good reason for hopping exclusively around the perimeter of the cluster as described in the definition of the given operator."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "q_env",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  },
  "orig_nbformat": 4,
  "vscode": {
   "interpreter": {
    "hash": "9181c3fee36b2a40316c7a7a7190b5ebac7b971b62240dc3c988739896c48274"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
