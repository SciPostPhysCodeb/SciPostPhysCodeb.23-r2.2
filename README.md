# Codebase release 2.2 for Pyqcm

by Théo N. Dionne, Alexandre Foley, Moïse Rousseau, David Sénéchal

SciPost Phys. Codebases 23-r2.2 (2023) - published 2023-12-20

[DOI:10.21468/SciPostPhysCodeb.23-r2.2](https://doi.org/10.21468/SciPostPhysCodeb.23-r2.2)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.23-r2.2) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Théo N. Dionne, Alexandre Foley, Moïse Rousseau, David Sénéchal.

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.23-r2.2](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.23-r2.2)
* Live (external) repository at [https://bitbucket.org/dsenechQCM/qcm_wed/src/v2.2.8/](https://bitbucket.org/dsenechQCM/qcm_wed/src/v2.2.8/)
